package com.awsecs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AWSECSServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AWSECSServiceApplication.class, args);
	}
}
